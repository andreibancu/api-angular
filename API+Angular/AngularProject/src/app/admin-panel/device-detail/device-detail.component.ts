import { Component, OnInit } from '@angular/core';
import { DeviceDetailService } from 'src/app/shared/device-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CategoryService } from 'src/app/shared/category.service';

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styles: []
})
export class DeviceDetailComponent implements OnInit {

  constructor(private service:DeviceDetailService,private toastr: ToastrService,private categoryService: CategoryService) { }

  ngOnInit() {
    this.resetForm();
    this.categoryService.refreshListCategory();
  }

  resetForm(form?:NgForm){

   if(form!=null) 
    form.form.reset();
   
    
    this.service.formData = {
      DeviceId:0,
      Name:'',
      Description:'',
      Price:0,
      AllCantity:0,
      CategoryId:0,
      imageName:'',
      Base64Image:''
    }
  }
  
  onSubmit(form:NgForm){
    this.service.postDeviceDetail(form.value).subscribe(
      res =>{
        
        this.resetForm(form);
        this.toastr.success('Submitted successfully!','Device Detail Register');
        this.service.refreshList();
      },
      err =>{
      
        console.log(err);
      }
    )
    
  }


  
}
