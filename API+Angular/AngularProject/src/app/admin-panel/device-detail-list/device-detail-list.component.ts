import { Component, OnInit } from '@angular/core';
import { DeviceDetailService } from 'src/app/shared/device-detail.service';
import { DeviceDetail } from 'src/app/shared/device-detail.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-device-detail-list',
  templateUrl: './device-detail-list.component.html',
  styles: []
})
export class DeviceDetailListComponent implements OnInit {

  constructor(private service: DeviceDetailService,private toastr: ToastrService) { }

  ngOnInit() {
    this.service.refreshList();
  }

  populateForm(pd:DeviceDetail){
     this.service.formData = Object.assign({},pd);
  }

  onDelete(deviceId){
    if(confirm('Are you sure to delete this record?')){
    this.service.deleteDeviceDetail(deviceId)
    .subscribe(res=>{
      this.service.refreshList();
      this.toastr.warning('Deleted successfully','Device Detail Deleted');
    },
      err=>{
        console.log(err);
      })
  }
  }
}
