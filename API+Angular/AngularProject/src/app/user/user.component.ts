import { Component, OnInit } from '@angular/core';
import { DeviceDetailService } from 'src/app/shared/device-detail.service';
import { DeviceDetail } from 'src/app/shared/device-detail.model';
import { CategoryService } from '../shared/category.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: []
})
export class UserComponent implements OnInit {

  constructor(private service: DeviceDetailService, private service2:CategoryService) { }

  ngOnInit() {
    this.service.refreshList();
    this.service2.refreshListCategory();
  }

}
