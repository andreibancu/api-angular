import { Injectable } from '@angular/core';
import { CheckOut } from './check-out.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CheckOutService {

  formCheckOut:CheckOut;

  readonly apiUrl = environment.apiUrl;

  listCheckOut: CheckOut[];

  constructor(private http:HttpClient) { }

  postCheckOut(formCheckOut:CheckOut){
    return this.http.post(this.apiUrl+'/Shippings',formCheckOut);
  }

  refreshListCheckOut(){
    this.http.get(this.apiUrl+'/Shippings')
    .toPromise()
    .then(res => this.listCheckOut = res as CheckOut[]);
 
  }
}
