import{DeviceDetail} from './device-detail.model';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {environment} from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})

export class DeviceDetailService {

  formData:DeviceDetail;

  
  readonly apiUrl = environment.apiUrl;
  list : DeviceDetail[];

  constructor(private http:HttpClient) { }

  postDeviceDetail(formData:DeviceDetail){
   return this.http.post(this.apiUrl+'/Devices/PostDevices',formData);
  }

  putDeviceDetail(formData:DeviceDetail){
    return this.http.put(this.apiUrl+'/Devices',formData);
   }
  deleteDeviceDetail(id){
    return this.http.delete(this.apiUrl+'/Devices/' +id);
   }

   getDeviceDetail(id:number){
   return this.http.get(this.apiUrl+'/Devices/'+id);
   }

  refreshList(){
    this.http.get(this.apiUrl+'/Devices')
    .toPromise()
    .then(res => this.list = res as DeviceDetail[]);
 
  }
}
