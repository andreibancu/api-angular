import { Injectable } from '@angular/core';
import { ItemDetail } from './item-detail.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ItemDetailService {

  itemData:ItemDetail;

  readonly apiUrl = environment.apiUrl;
  itemList:ItemDetail[];

  constructor(private http:HttpClient) { }

  putItemDetail(){
    let quantity = 1;
    let idItem = 1
    let idDevice =5;
    return this.http.put(this.apiUrl+'/Items',{idItem,quantity,idDevice});
   }

}
