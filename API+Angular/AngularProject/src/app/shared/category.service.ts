import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category } from './category.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  readonly apiUrl = environment.apiUrl;

  listCategory: Category[];

  constructor(private http:HttpClient) { }

  refreshListCategory(){
    this.http.get(this.apiUrl+'/Category')
    .toPromise()
    .then(res => this.listCategory = res as Category[]);
 
  }
}
