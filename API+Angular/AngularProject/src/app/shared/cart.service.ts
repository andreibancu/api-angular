import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CartDetail } from './cart-detail.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {
 
  cartData:CartDetail;

  readonly apiUrl = environment.apiUrl;
  cartList:CartDetail[];

  constructor(private http: HttpClient) { }

  postCart(cartData:CartDetail){
    return this.http.post(this.apiUrl+'/Carts',{dateCreated:new Date().getTime()});
  }


}
