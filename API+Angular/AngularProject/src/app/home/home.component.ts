import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceDetailService } from 'src/app/shared/device-detail.service';
import { UserService } from '../shared/user.service';
import {environment} from "src/environments/environment";
import { DeviceDetail } from 'src/app/shared/device-detail.model';
import {CartService} from 'src/app/shared/cart.service'
import{ItemDetailService} from 'src/app/shared/item-detail.service'
import { CategoryService } from '../shared/category.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  userDetails;

  constructor(private router:Router,private service: DeviceDetailService, private cartService: CartService,private item: ItemDetailService,private categoryService: CategoryService) { }

  ngOnInit() {
    this.service.refreshList();
    this.categoryService.refreshListCategory();

  /*  this.service.getUserProfile().subscribe(
      res =>{
        this.userDetails = res;
      },
      err =>{
        console.log(err);
      },
    ); */
  }

  addToCart(){
    let itterator = 0;
    return itterator +1;
    
  }

  onLogout(){
    localStorage.removeItem('token');
    this.router.navigate(['/user/login']);
  }

}
