import { Component, OnInit } from '@angular/core';
import { CheckOutService } from '../shared/check-out.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  constructor(private service:CheckOutService) { }

  ngOnInit() {
    this.service.refreshListCheckOut();
  }

}
