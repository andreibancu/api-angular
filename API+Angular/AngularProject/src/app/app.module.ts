import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule,FormsModule} from '@angular/forms'
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserService } from './shared/user.service';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { DeviceDetailComponent } from './admin-panel/device-detail/device-detail.component';
import { DeviceDetailListComponent } from './admin-panel/device-detail-list/device-detail-list.component';
import { DeviceDetailService } from './shared/device-detail.service';
import { CartService } from './shared/cart.service';
import { ItemDetailService } from './shared/item-detail.service';
import { ShoppingCartComponent } from './app/shopping-cart/shopping-cart.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { CheckOutService } from './shared/check-out.service';
import { TransactionsComponent } from './transactions/transactions.component';
import {CategoryService} from './shared/category.service'

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    AdminPanelComponent,
    ForbiddenComponent,
    DeviceDetailComponent,
    DeviceDetailListComponent,
    ShoppingCartComponent,
    CheckOutComponent,
    TransactionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      progressBar:true
    }),
    FormsModule
    
  ],
  providers: [UserService,{
    provide: HTTP_INTERCEPTORS,
    useClass:AuthInterceptor,
    multi:true
  },DeviceDetailService,
    CartService,
  ItemDetailService,
  CheckOutService,
  CategoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
