import { Component, OnInit } from '@angular/core';
import { CheckOutService } from '../shared/check-out.service';
import{NgForm} from '@angular/forms'

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {

  constructor(private service:CheckOutService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?:NgForm){

    if(form!=null) 
     form.form.reset();
    
     
     this.service.formCheckOut = {
       ShippingId:0,
       Name:'',
       Address:'',
       City:'',
       
     }
   }
   onSubmit(form:NgForm){
    this.service.postCheckOut(form.value).subscribe(
      res =>{
        
        this.resetForm(form);
       
      },
      err =>{
      
        console.log(err);
      }
    )
    
  }
  
}

