﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiMobileShop.ViewModels
{
    public class TransactionViewModel
    {
        public int Cantity { get; set; }

        public string Address { get; set; }

        public string Buyer { get; set; }

        public string Email { get; set; }
    }
}
