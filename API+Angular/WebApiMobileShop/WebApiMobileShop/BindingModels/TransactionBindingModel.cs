﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.Models;

namespace WebApiMobileShop.BindingModels
{
    public class TransactionBindingModel
    {
        public int TransactionId { get; set; }

        public int DeviceId { get; set; }

        public int Cantity { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public virtual Device Device { get; set; }
    }
}
