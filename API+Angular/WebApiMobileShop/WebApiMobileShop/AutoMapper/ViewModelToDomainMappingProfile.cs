﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.BindingModels;
using WebApiMobileShop.Models;

namespace WebApiMobileShop.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "ViewModelToDomainMappingProfile";
            }
        }

        public ViewModelToDomainMappingProfile()
        {
            ConfigureMappings();
        }


        /// <summary>
        /// Creates a mapping between source (ViewModel) and destination (Domain)
        /// </summary>
        private void ConfigureMappings()
        {
            CreateMap<DeviceBindingModel, Device>().ReverseMap();
            CreateMap<TransactionBindingModel, Transaction>().ReverseMap();

        }
    }
}
