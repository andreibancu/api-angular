﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.Models;
using WebApiMobileShop.ViewModels;

namespace WebApiMobileShop.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "DomainToViewModelMappings";
            }
        }

        public DomainToViewModelMappingProfile()
        {
            ConfigureMappings();
        }


        /// <summary>
        /// Creates a mapping between source (Domain) and destination (ViewModel)
        /// </summary>
        private void ConfigureMappings()
        {
            CreateMap<Device, DeviceViewModel>().ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category.Name)).ReverseMap();
            CreateMap<Transaction, TransactionViewModel>().ReverseMap();

        }
    }
}
