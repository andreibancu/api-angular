﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiMobileShop.Models
{
    public class Item
    {
        public int ItemId { get; set; }

        public int Quantity { get; set; }

        public int DeviceId { get; set; }

        public virtual Device Device { get; set; }

    }
}
