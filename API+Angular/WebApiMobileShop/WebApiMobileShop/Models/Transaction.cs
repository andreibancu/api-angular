﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiMobileShop.Models
{
    public class Transaction
    {
        public int TransactionId { get; set; }

        public int DeviceId { get; set; }

        public int Cantity { get; set; }

        public string Adress { get; set; }

        public string Buyer { get; set; }

        public string Email { get; set; }

        public virtual Device Device { get; set; }
    }
}
