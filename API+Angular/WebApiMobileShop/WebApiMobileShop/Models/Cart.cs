﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiMobileShop.Models
{
    public class Cart
    {
        public int CartId { get; set; }

        public DateTime DateCreated { get; set; }

        public int ItemId { get; set; }

        public virtual Item Item { get; set; }

    }
}
