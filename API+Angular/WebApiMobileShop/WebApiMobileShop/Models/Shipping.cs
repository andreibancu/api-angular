﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiMobileShop.Models
{
    public class Shipping
    {
        public int ShippingId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }
    }
}
