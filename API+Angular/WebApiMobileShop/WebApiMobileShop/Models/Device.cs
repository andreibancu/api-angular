﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace WebApiMobileShop.Models
{
    public class Device
    {
        public int DeviceId { get; set; }

        public string Name { get; set; }

        public int AllCantity { get; set; }

        public string Description { get; set; }

        public string ImageName { get; set; }

        public decimal Price { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
    }
}
