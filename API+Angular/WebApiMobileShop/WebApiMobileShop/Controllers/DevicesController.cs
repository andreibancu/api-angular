﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiMobileShop.BindingModels;
using WebApiMobileShop.Models;
using WebApiMobileShop.Repository.Interfaces;
using WebApiMobileShop.ViewModels;

namespace WebApiMobileShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicesController : ControllerBase
    {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly IMapper _mapper;


        public DevicesController(IRepositoryWrapper repositoryWrapper, IMapper mapper)
        {
            _repositoryWrapper = repositoryWrapper;
            _mapper = mapper;
        }

        // GET: api/Devices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeviceViewModel>>> GetDevices(string searchText = "", int categoryId = 0)
        {
            var devices = new List<Device>();
            var devicesViewModels = new List<DeviceViewModel>();
            try
            {
                if (string.IsNullOrEmpty(searchText))
                {
                    searchText = string.Empty;
                }

                if (categoryId == 0)
                {
                    devices = await _repositoryWrapper.DeviceRepository.FindByCondition(c => c.Name.Contains(searchText)).Include(c => c.Category).ToListAsync();
                }
                else
                {
                    devices = await _repositoryWrapper.DeviceRepository.FindByCondition(c => c.Name.Contains(searchText) && c.CategoryId == categoryId).Include(device => device.Category).ToListAsync();
                }

                devicesViewModels = _mapper.Map<List<DeviceViewModel>>(devices);

                // should move this in a service
                foreach (var deviceViewModel in devicesViewModels)
                {
                    if (deviceViewModel.ImageName != null)
                    {
                        deviceViewModel.Base64Image = ConvertImageToBase64String(deviceViewModel.ImageName);
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return devicesViewModels;
        }

   

        // GET: api/Devices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DeviceViewModel>> GetDevice(int id)
        {
            try
            {

                var device = _repositoryWrapper.DeviceRepository.FindAll().Include(c => c.Category).ToListAsync().Result.FirstOrDefault(c => c.DeviceId == id);
                var deviceViewModel = _mapper.Map<DeviceViewModel>(device);
                deviceViewModel.Base64Image = ConvertImageToBase64String(deviceViewModel.ImageName);



                if (device == null)
                {
                    return NotFound();
                }

                return deviceViewModel;
            }
            catch (Exception ex)
            {

            }

            return NotFound();
        }

        // POST: api/Devices
        [HttpPost]
        [Route("PostDevices")]
        public async Task<ActionResult<Device>> PostDevice(IFormFile image, [FromForm] DeviceBindingModel deviceBindingModel)
        {
            try
            {
                var device = _mapper.Map<Device>(deviceBindingModel);

                if (image != null && image.Length > 0)
                {
                    string fileName = DateTime.Now.ToString("ddMMyyhhmmss") + image.FileName;
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images", fileName);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await image.CopyToAsync(stream);
                        device.ImageName = fileName;
                    }
                }

                _repositoryWrapper.DeviceRepository.Create(device);
                _repositoryWrapper.Save();


                return CreatedAtAction(nameof(GetDevice), new { id = device.DeviceId }, device);
            }
            catch (Exception ex)
            {

            }

            return BadRequest();
        }


        // PUT: api/Devices/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDevice(long id, Device device)
        {
            if (id != device.DeviceId)
            {
                return BadRequest();
            }

            var deviceToUpdate = _repositoryWrapper.DeviceRepository.FindByCondition(c => c.DeviceId == id).FirstOrDefault();
            _repositoryWrapper.DeviceRepository.Update(deviceToUpdate);

            return NoContent();
        }

        // DELETE: api/Devices/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDevice(int id)
        {
            var device = _repositoryWrapper.DeviceRepository.FindByCondition(c => c.DeviceId == id).FirstOrDefault();

            if (device == null)
            {
                return NotFound();
            }

            _repositoryWrapper.DeviceRepository.Delete(device);
            _repositoryWrapper.Save();

            return NoContent();
        }

        private string ConvertImageToBase64String(string imageName)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images", imageName);
            if (System.IO.File.Exists(path))
            {
                byte[] b = System.IO.File.ReadAllBytes(path);
                return "data:image/jpg;base64," + Convert.ToBase64String(b);
            }

            return string.Empty;
        }
    }
}




//private readonly AuthenticationContext _context;

//public DevicesController(AuthenticationContext context)
//{
//    _context = context;
//}

//// GET: api/Devices
//[HttpGet]
//public async Task<ActionResult<IEnumerable<Device>>> GetDevices()
//{
//    return await _context.Devices.ToListAsync();
//}

//// GET: api/Devices/5
//[HttpGet("{id}")]
//public async Task<ActionResult<Device>> GetDevice(int id)
//{
//    var device = await _context.Devices.FindAsync(id);

//    if (device == null)
//    {
//        return NotFound();
//    }

//    return device;
//}

//// PUT: api/Devices/5
//[HttpPut("{id}")]
//public async Task<IActionResult> PutDevice(int id, Device device)
//{
//    if (id != device.DeviceId)
//    {
//        return BadRequest();
//    }

//    _context.Entry(device).State = EntityState.Modified;

//    try
//    {
//        await _context.SaveChangesAsync();
//    }
//    catch (DbUpdateConcurrencyException)
//    {
//        if (!DeviceExists(id))
//        {
//            return NotFound();
//        }
//        else
//        {
//            throw;
//        }
//    }

//    return NoContent();
//}

//// POST: api/Devices
//[HttpPost]
//public async Task<ActionResult<Device>> PostDevice(Device device)
//{
//    _context.Devices.Add(device);
//    await _context.SaveChangesAsync();

//    return CreatedAtAction("GetDevice", new { id = device.DeviceId }, device);
//}

//// DELETE: api/Devices/5
//[HttpDelete("{id}")]
//public async Task<ActionResult<Device>> DeleteDevice(int id)
//{
//    var device = await _context.Devices.FindAsync(id);
//    if (device == null)
//    {
//        return NotFound();
//    }

//    _context.Devices.Remove(device);
//    await _context.SaveChangesAsync();

//    return device;
//}

//private bool DeviceExists(int id)
//{
//    return _context.Devices.Any(e => e.DeviceId == id);
//}