﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.Models;
using WebApiMobileShop.Repository.Interfaces;

namespace WebApiMobileShop.Repository
{
    public class TransactionRepository : RepositoryBase<Transaction>, ITransactionRepository
    {
        public TransactionRepository(AuthenticationContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}
