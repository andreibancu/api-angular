﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.Repository.Interfaces;
using WebApiMobileShop.Models;

namespace WebApiMobileShop.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private AuthenticationContext _applicationDbContext;
        private IDeviceRepository _deviceRepository;
        private ICategoryRepository _categoryRepository;
        private ITransactionRepository _transactionRepository;

        public IDeviceRepository DeviceRepository
        {
            get
            {
                if (_deviceRepository == null)
                {
                    _deviceRepository = new DeviceRepository(_applicationDbContext);
                }

                return _deviceRepository;
            }
        }

        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_applicationDbContext);
                }

                return _categoryRepository;
            }
        }


        public ITransactionRepository TransactionRepository
        {
            get
            {
                if (_transactionRepository == null)
                {
                    _transactionRepository = new TransactionRepository(_applicationDbContext);
                }

                return _transactionRepository;
            }
        }

        public RepositoryWrapper(AuthenticationContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Save()
        {
            _applicationDbContext.SaveChanges();
        }
    }
}
