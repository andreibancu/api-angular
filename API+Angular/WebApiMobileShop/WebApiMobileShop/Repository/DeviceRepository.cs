﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.Models;
using WebApiMobileShop.Repository.Interfaces;

namespace WebApiMobileShop.Repository
{
    public class DeviceRepository : RepositoryBase<Device>, IDeviceRepository
    {
        public DeviceRepository(AuthenticationContext applicationDbContext)
            : base(applicationDbContext)
        {

        }
    }
}
