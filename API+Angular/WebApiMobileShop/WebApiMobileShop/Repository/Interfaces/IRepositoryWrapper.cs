﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiMobileShop.Repository.Interfaces
{
    public interface IRepositoryWrapper
    {
        IDeviceRepository DeviceRepository { get; }

        ICategoryRepository CategoryRepository { get; }

        ITransactionRepository TransactionRepository { get; }

        void Save();
    }
}
