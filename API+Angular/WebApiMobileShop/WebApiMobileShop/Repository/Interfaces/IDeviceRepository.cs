﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.Models;

namespace WebApiMobileShop.Repository.Interfaces
{
    public interface IDeviceRepository : IRepositoryBase<Device>
    {
    }
}
