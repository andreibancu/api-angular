﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMobileShop.Models;
using WebApiMobileShop.Repository.Interfaces;

namespace WebApiMobileShop.Repository
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(AuthenticationContext applicationDbContext)
           : base(applicationDbContext)
        {
        }
    }
}
